import java.util.Scanner;
public class latihanswitch {
     public static void main(String[] args) {
     
        Scanner input = new Scanner(System.in);
         // input operator dengan tipe data char
         System.out.print("Choose an operator : +, -, *, or /\n");
         char operator = input.next().charAt(0);;
         //input angka dengan tipe data double
         System.out.print("Enter first number: ");
         double first = input.nextDouble();
         System.out.print("Enter second number: ");
         double second = input.nextDouble();
         
         double hasil;
         
         //membuat pengkondisian operator
         switch (operator) {
             case '+':
             hasil = first + second;
             System.out.print(first +"+"+ second +" = "+ hasil);
             break;
             
             case '-':
             hasil = first - second;
             System.out.print(first + "-" +second +" = "+ hasil);
             break;
             
             case '*':
             hasil = first * second;
             System.out.print(first +"*"+ second +" = "+ hasil);
             break;
             
             case '/':
             hasil = first / second;
             System.out.print(first + "/" + second +" = "+ hasil);
             break;
             
             default:
             System.out.print("Salah Input Operator!");
             break;
             }
        
     }
}
