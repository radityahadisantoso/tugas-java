import java.util.Scanner;

public class IfStatement {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Input Data
        System.out.print("Nama = ");
        String nama = input.nextLine();
        System.out.print("Tanggal Lahir = ");
        String tanggal = input.nextLine();
        System.out.print("Bulan Lahir = ");
        String bulan = input.nextLine();
        System.out.print("Tahun = ");
        String tahun = input.nextLine();

        String namaBulan;
        int umur = 2022 - Integer.parseInt(tahun);
        
        // Mencari Nama bulan dari data inputan integer
        if (bulan.equals("1")) {
            namaBulan = "Januari";
        } else if (bulan.equals("2")) {
            namaBulan = "Februari";
        } else if (bulan.equals("3")) {
            namaBulan = "Maret";
        } else if (bulan.equals("4")) {
            namaBulan = "April";
        } else if (bulan.equals("5")) {
            namaBulan = "Mei";
        } else if (bulan.equals("6")) {
            namaBulan = "Juni";
        } else if (bulan.equals("7")) {
            namaBulan = "Juli";
        } else if (bulan.equals("8")) {
            namaBulan = "Agustus";
        } else if (bulan.equals("9")) {
            namaBulan = "September";
        } else if (bulan.equals("10")) {
            namaBulan = "Oktober";
        } else if (bulan.equals("11")) {
            namaBulan = "November";
        } else if (bulan.equals("12")) {
            namaBulan = "Desember";
        } else {
            namaBulan = "Tidak ada";
        }

        // Cetak data
        System.out.println("Nama saya " + nama + ", lahir " + tanggal + " " + namaBulan + " " + tahun + " berumur "
                + umur + " tahun");

    }
}
