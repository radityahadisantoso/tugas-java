import java.util.Scanner;
public class TugasArray{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        
        //input panjang array
        System.out.print("Input Panjang Array : ");
        int lengthIndex = input.nextInt();
        
        //mendefinisikan dan menginisialisasikan array
        int [] iArray = new int [lengthIndex];
        
        //perulangan panjang array
        for(int i=0; i<iArray.length; i++){
            
            //cetak urutan index
            System.out.print("Index ["+i+"] = ");
            //input nilai index
            iArray[i] = input.nextInt();
        }
        //cetak panjang array
        System.out.println("Panjang Array Integer adalah "+lengthIndex);
        System.out.println(" ");
        System.out.print("Array Integer {");
        
        //perulangan untuk cetak nilai array yang ada di index
        for(int a : iArray){
            System.out.print(" "+a+" ");
        }
        System.out.print("}");
     
        
     }
}