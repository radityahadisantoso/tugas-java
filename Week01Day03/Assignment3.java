
import java.util.Scanner;
public class LatihanShortSearch{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        
        //input panjang array
        System.out.print("Input Panjang Array : ");
         int lengthIndex = input.nextInt();
         
         //mengidentifikasi array
         int [] a = new int [lengthIndex];

            //perulangan untuk input nilai array
            for(int i=0; i<a.length; i++){
                    System.out.print("Index ["+i+"] :");
                    a[i] = input.nextInt();
                }
                
        int w=1, loop=2;
        int minPos; 
        while(w<loop){
         
         System.out.println("Menu ");
         System.out.println("1. Bubble Sort ");
         System.out.println("2. Selection Sort ");
         System.out.println("3. Cari data ");
         System.out.println("");
         System.out.print("Input nomor : ");
         int menu = input.nextInt();
            switch (menu) {

                //perulangan sorting menggunakan bubble sort
                case 1:
                    for (int i = 0; i < a.length - 1; i++) {
                         for (int j = 0; j < a.length - 1 - i; j++) {
                             if (a[j + 1] < a[j]) {
                                 int temp = a[j];
                                 a[j] = a[j + 1];
                                 a[j + 1] = temp;
                                 }
                         }
                    }
                    for(int cari : a){
                        System.out.print(" "+cari+" ");
                    }
                break;
                
                //perulangan sorting menggunakan selection sort
                case 2: 
                    for (int i = 0; i < a.length; i++){
                         minPos = i;
                        for (int j = i + 1; j < a.length; j++){
                            if (a[j] < a[minPos]){
                                minPos = j;
                            }
                        }
                     int temp = a[i];
                     a[i] = a[minPos];
                     a[minPos] = temp;
                    }
                    for(int cari : a){
                        System.out.print(" "+cari+" ");
                    }
                
                //cari target menggunakan  binary search
                case 3:
                    System.out.println("Cari Target : ");
                    int target = input.nextInt();
                    int left = 0;
                    int middle;
                    int right = a.length - 1;
                    while (left <= right) {
                         middle = (left + right) / 2;
                         if (a[middle] == target) {
                         System.out.println("Element "+target+ " at index " + middle);
                         break;
                         } else if (a[middle] < target) {
                         left = middle + 1;
                         } else if (a[middle] > target) {
                         right = middle - 1;
                         }
                    }
                default:
                     System.out.print("Salah input Menu, Menu");
                break;
            }    
        
             
        }
        
     }
     
}