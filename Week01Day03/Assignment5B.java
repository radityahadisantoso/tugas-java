import java.util.*; 
public class HelloWorld{

    public static void main(String []args){
    Scanner input= new Scanner(System.in);
    Scanner inputInt= new Scanner(System.in);

    //membuat map
    Map<Integer,String> map=new HashMap<Integer,String>(); 
     //Adding elements to map 
    for (int i=1;i<=3;i++){
        System.out.print("Input Number " + i + ": ");
        int num = inputInt.nextInt();
        System.out.print("Input Nama " + i + ": ");
        String name = input.nextLine();
        map.put(num,name);
    }
    //mengkonversi untuk mendapatkan key dan value kemudian di tampilkan
   Set set=map.entrySet();
   Iterator itr=set.iterator();
   while(itr.hasNext()){
       Map.Entry entry=(Map.Entry)itr.next();
       System.out.println(entry.getKey()+"   "+entry.getValue());
   }
   //membuat perulangan pilih menu
    int w=1, loop=2;
    int num;
    String name;
        while(w<loop){
            System.out.println("MENU ");
            System.out.println("1. Tambah Nama");
            System.out.println("2. Hapus Nama ");
            System.out.println("3. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");
            
            //mendeklarasikan switch menu
            int menu = inputInt.nextInt();
            switch (menu) {
                case 1:
                    //menambahkan 1 data kemudian di tampilkan
                    System.out.print("Input Number : ");
                    num = inputInt.nextInt();
                    System.out.print("Input Nama : ");
                    name = input.nextLine();
                    map.put(num,name);
                    Set set1=map.entrySet();
                    Iterator itr1=set1.iterator();
                    while(itr1.hasNext()){
                    Map.Entry entry=(Map.Entry)itr1.next();
                    System.out.println(entry.getKey()+"   "+entry.getValue());
            
                    } 
                case 2:
                    //menginput 1 data yang akan di hapus
                    System.out.print("Input Number : ");
                    num = inputInt.nextInt();
                    System.out.print("Input Nama : ");
                    name = input.nextLine();
                    map.remove(num,name);
                    
                    Set set2=map.entrySet();
                    Iterator itr2=set2.iterator();
                    while(itr2.hasNext()){
                    Map.Entry entry=(Map.Entry)itr2.next();
                    System.out.println(entry.getKey()+"   "+entry.getValue());
                
                default :
                    //mengubah variabel parameter untuk berhenti perulangan input menu
                    System.out.println("== Anda telah Exit ==");
                    w= 10;
                    inputInt.close();
                    input.close();
                    
                break;
            
                    } 
                    
            }

        }
  }
}