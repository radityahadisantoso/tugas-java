import java.util.*; 
public class Assignment1{

     public static void main(String []args){
        
        Scanner input= new Scanner(System.in);
        Scanner inputInt= new Scanner(System.in);
        
        //mendeklarasikan array list untuk menampung data
        ArrayList<String> nama= new ArrayList<String>();
        //mendeklarasikan array list untuk menampung data yang akan di remove dan retain
        ArrayList<String> hapusNama= new ArrayList<String>();
        
        //menginput data kedalam arraylist
        System.out.print("Input Nama ke 1 : ");
        nama.add(input.nextLine());
        System.out.print("Input Nama ke 2 : ");
        nama.add(input.nextLine());
        System.out.print("Input Nama ke 3 : ");
        nama.add(input.nextLine());
        
        //menampilkan data nama
        Iterator itr=nama.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }

        //perulangan  untuk pilih menu
        int w=1, loop=2;
        while(w<loop){
            System.out.println("MENU ");
            System.out.println("1. Tambah Nama");
            System.out.println("2. Hapus Nama ");
            System.out.println("3. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");
            
            //mendeklarasikan switch menu
            int menu = inputInt.nextInt();
            switch (menu) {
                
                
                case 1:
                    //menambahkan data nama
                    System.out.print("Input Nama : ");
                    nama.add(input.nextLine());
                    Iterator vi=nama.iterator();
                    while(vi.hasNext()){
                        System.out.println(vi.next());
                    }
                break;
                
                case 2:
                    //menambahkan data nama untuk di remove
                    System.out.print("Hapus Nama : ");
                    hapusNama.add(input.nextLine());
                    nama.removeAll(hapusNama);
                    
                    Iterator vo=nama.iterator();
                    while(vo.hasNext()){
                        System.out.println(vo.next());
                    }
                break;
                
                case 3:

                    //menambahkan data nama untuk di retain
                    System.out.print("Retain Nama ke 1 : ");
                    hapusNama.add(input.nextLine());
                    System.out.print("Retain Nama ke 2 : ");
                    hapusNama.add(input.nextLine());
                    nama.retainAll(hapusNama);
                    
                    Iterator vu=nama.iterator();
                    while(vu.hasNext()){
                        System.out.println(vu.next());
                    }
                
                default :
                    //mengubah variabel parameter untuk berhenti perulangan input menu
                    System.out.println("== Anda telah Exit ==");
                    w= 10;
                    inputInt.close();
                    input.close();
                    
                break;
                    
            }    
        }
            
     }
}