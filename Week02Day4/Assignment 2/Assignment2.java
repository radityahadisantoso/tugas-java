package comp.assignment2;

import java.util.*;
public class Assignment2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        //perualngan untuk pilih menu operator
        int w = 1, loop = 2, num1=0, num2=0;
        while (w < loop) {
            System.out.println("MENU ");
            System.out.println("1. Penjumlahan");
            System.out.println("2. Pengurangan ");
            System.out.println("3. Perkalian");
            System.out.println("4. Pembagian");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");

            int menu = input.nextInt();
            switch (menu) {
                case 1:
                    //input bilangan yang akan di jumlahkan
                    System.out.print("Input Number ke 1 : ");
                    num1= input.nextInt();
                    System.out.print("Input Number ke 2 : ");
                    num2= input.nextInt();

                    //membuat object untuk memanggil method penjumlahan
                    Calculate hasil= new Calculate();
                    int result = hasil.Penjumlahan(num1,num2);
                    System.out.println(num1+" + "+num2+" = " + result);
                    break;

                case 2:
                    //input bilangan yang akan di kurangkan
                    System.out.print("Input Number ke 1 : ");
                    num1= input.nextInt();
                    System.out.print("Input Number ke 2: ");
                    num2= input.nextInt();

                    //membuat object untuk memanggil method pengurangan
                    Calculate hasil1= new Calculate();
                    result = hasil1.Pengurangan(num1,num2);
                    System.out.println(num1+" - "+num2+" = " + result);
                case 3:
                    //input bilangan yang akan di kalikan
                    System.out.print("Input Number ke 1 : ");
                    num1= input.nextInt();
                    System.out.print("Input Number ke 2: ");
                    num2= input.nextInt();

                    //membuat object untuk memanggil method perkalian
                    Calculate hasil2= new Calculate();
                    result = hasil2.Perkalian(num1,num2);
                    System.out.println(num1+" * "+num2+" = " + result);

                case 4:
                    //input bilangan yang akan di bagi
                    System.out.print("Input Number ke 1 : ");
                    double numm= input.nextDouble();
                    System.out.print("Input Number ke 2: ");
                    double numb= input.nextDouble();

                    //membuat object untuk memanggil method pembagian
                    Calculate hasil3= new Calculate();
                    double result1 = hasil3.Pembagian(numm,numb);
                    System.out.println(numm+" / "+numb+" = " + result1);
                    break;
                default:
                    System.out.println("== Anda telah Exit ==");
                    //mengubah variabel parameter untuk berhenti perulangan input menu
                    w = 10;
                    input.close();
                    break;
            }
        }

    }
}
