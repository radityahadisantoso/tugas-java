package comp.assignment5;

public class Mahasiswa {
        //mendeklarasikan variabel untuk menampung data
        String nama;
        int nilaiFisika, nilaiKimia, nilaiBiologi;
        public Mahasiswa(String nama, int fisika, int biologi, int kimia) {
            this.nama = nama;
            this.nilaiBiologi = biologi;
            this.nilaiFisika = fisika;
            this.nilaiKimia = kimia;
        }
        //membuat method get Mahasiswa
        public String getMahasiswa() {
            return this.nama + "," + this.nilaiBiologi + "," + this.nilaiFisika + "," + this.nilaiKimia;
        }
    }

