package comp.assignment5;
import java.util.Scanner;
public class assignment5 {
    public static void main(String[] args) {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);
        //mendeklarasikan object data untuk class mahasiswa
        Mahasiswa data = new Mahasiswa("", 0, 0, 0);
        int kimia, fisika, biologi;
        String nama, dataMahasiswa = "";

        //perulangan untuk pilih menu
        int w = 1, loop = 2;
        while (w < loop) {
            System.out.println("Menu");
            System.out.println("1. Input data mahasiswa ");
            System.out.println("2. Parsing and print data");
            System.out.println("3. EXIT");
            System.out.print("Pilih Menu = ");
            int menu = inputI.nextInt();
            switch (menu) {
                case 1:
                    //input data mahasiswa
                    System.out.println("Input data mahasiswa ");
                    System.out.print("Input Nama = ");
                    nama = inputS.nextLine();
                    System.out.print("Input Nilai Fisika = ");
                    fisika = inputI.nextInt();
                    System.out.print("Input Nilai Biologi = ");
                    biologi = inputI.nextInt();
                    System.out.print("Input Nilai Kimia = ");
                    kimia = inputI.nextInt();

                    //membuat object untuk menginput data ke class mahasiswa
                    data = new Mahasiswa(nama, fisika, biologi, kimia);
                    //object untuk menampung object data mahasiswa
                    dataMahasiswa = data.getMahasiswa();
                    //cetak data mahasiswa
                    System.out.println(dataMahasiswa);
                    break;
                case 2:

                    System.out.println("Parsing and print data ");
                    //mendeklarasikan dan menginisialisasi variabel string dengan object
                    String dataString = data.getMahasiswa();
                    //membuat method split dengan parameter ","
                    String[] arrMhs = dataString.split(",");
                    //perulangan untuk menampilkan data dengan parameter index
                    for (int i = 0; i < arrMhs.length; i++) {
                        if (i == 0) {
                            System.out.println(arrMhs[i]);
                        } else {
                            System.out.print(arrMhs[i] + " ");
                        }
                    }
                    System.out.println("");
                    break;
                default:
                    System.out.println("Masukan pilihan yang sesuai");
                    break;
            }
        }
    }
}


